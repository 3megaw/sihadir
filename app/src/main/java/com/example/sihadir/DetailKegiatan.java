package com.example.sihadir;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.sihadir.Firebase.DataKegiatan;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class DetailKegiatan extends AppCompatActivity {

    DatabaseReference databaseReference;
    TextView isi,judul,tanggal,tempat,waktumulai,waktuakhir,txttag;
    static String tampung;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kegiatan);

        isi = findViewById(R.id.isi);
        judul = findViewById(R.id.judul);
        tanggal = findViewById(R.id.tanggal);
        tempat = findViewById(R.id.tempat);
        waktumulai = findViewById(R.id.waktu);
        waktuakhir = findViewById(R.id.waktu);
        txttag = findViewById(R.id.tagnama);

        //GET DATA
        Intent i = this.getIntent();
        String gjudul=i.getExtras().getString("judul");
        String tgl=i.getExtras().getString("tanggal");
        String kode=i.getExtras().getString("key");
        String gisi=i.getExtras().getString("isi");
        String wktm=i.getExtras().getString("wktm");
        String wkta=i.getExtras().getString("wkta");
        String tmp=i.getExtras().getString("tempat");
        String tagn=i.getExtras().getString("tag");

        judul.setText(gjudul);
        tanggal.setText(tgl);
        tempat.setText(tmp);
        isi.setText(gisi);
        waktumulai.setText(wktm);
        waktuakhir.setText(wkta);
        txttag.setText(tagn);
    }

    /*
    private void setData()
    {
        databaseReference = FirebaseDatabase.getInstance().getReference("Kegiatan");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    DataKegiatan dataKegiatan = postSnapshot.getValue(DataKegiatan.class);
                    if(tampung.equals(dataKegiatan.getKode()))
                    {
                        isi.setText(dataKegiatan.getIsi());
                        judul.setText(dataKegiatan.getJudul());
                        tanggal.setText(dataKegiatan.getTanggal());
                        tempat.setText(dataKegiatan.getTempat());
                        waktuakhir.setText(dataKegiatan.getWaktumulai() + "-" + dataKegiatan.getWaktuakhir());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

     */

    private String setTanggal(String tanggal)
    {
        int a = 0;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date data = null;

        try {
            data = simpleDateFormat.parse(simpleDateFormat.format(calendar.getTime()));
        }
        catch (ParseException e){
            e.printStackTrace();
        }
        return  simpleDateFormat.format(data);
    }



}
