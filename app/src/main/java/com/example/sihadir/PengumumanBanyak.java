package com.example.sihadir;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PengumumanBanyak extends AppCompatActivity {

    private List<DataPengumuman> data = new ArrayList<>();
    private DatabaseReference databaseReference;
    RecyclerView recyclerView;
    ListDaftarPengumuman listDaftarPengumuman;

    TextView txttgl,txtjudul,txtisi,txtcount;
    DataPengumuman dataPengumuman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengumuman_banyak);

        txtcount = findViewById(R.id.txtcount);
        onDaftar();
    }
    private void onDaftar(){
        recyclerView = findViewById(R.id.viewdaftar);
        listDaftarPengumuman = new ListDaftarPengumuman(this,data);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(listDaftarPengumuman);

        databaseReference = FirebaseDatabase.getInstance().getReference("Pengumuman");
        databaseReference.addValueEventListener(new ValueEventListener() {
            int x=0;
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    DataPengumuman dape =postSnapshot.getValue(DataPengumuman.class);
                    x= x+(int)postSnapshot.getChildrenCount();
                    data.add(dape);
                    Collections.reverse(data);
                }
                listDaftarPengumuman.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
