package com.example.sihadir;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sihadir.Firebase.DataKehadiranApelPagi;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HistoryApelPagi extends RecyclerView.Adapter<HistoryApelPagi.NamaViewHolder> {
    private DatabaseReference databaseReference;
    DatabaseReference databaseReference2;
    DatabaseReference databaseReference3;
    DatabaseReference databaseReference4;

    String getKeyNip;

    Menu menu;
    String kl = menu.getKeyNip;

    @NonNull
    @Override
    public NamaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.list_kegiatan,viewGroup,false);
        return new NamaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NamaViewHolder namaViewHolder, int i) {
        databaseReference = FirebaseDatabase.getInstance().getReference("Kehadiran Apel Pagi");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    databaseReference3 = databaseReference2.child(postSnapshot.getKey());
                    databaseReference3.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            for (DataSnapshot ff : dataSnapshot.getChildren())
                            {
                                DataKehadiranApelPagi dataKehadiranApelPagi6 = ff.getValue(DataKehadiranApelPagi.class);
                                if(dataKehadiranApelPagi6.getNip().equals(getKeyNip) )
                                {
                                   //yy[0]=yy[0]+dataKehadiranApelPagi6.getPoin();
                                    data.add(dataKehadiranApelPagi6);
                                }
                                //txtpengumuman.setText(dataKehadiranApelPagi6.getNip());
                            }
            //z                persentase.setText(""+yy[0]);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        DataKehadiranApelPagi dataKehadiranApelPagi = data.get(i);
        konek_datadiri konekDatadiri=new konek_datadiri();


        if(dataKehadiranApelPagi.getStatus().equals("Hadir"))
        {
            namaViewHolder.textViewTanggal.setText(getHari(dataKehadiranApelPagi.getTanggal()));
            namaViewHolder.textViewTitle.setText("Anda Memasuki Apel Pagi tepat waktu  ");
            String uri = "@drawable/tepat";  // where myresource (without the extension) is the file
            int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
            Drawable res = context.getResources().getDrawable(imageResource);
            namaViewHolder.imgIcon.setImageDrawable(res);
        }
        else if(dataKehadiranApelPagi.getStatus().equals("Telat"))
        {
            namaViewHolder.textViewTanggal.setText(getHari(dataKehadiranApelPagi.getTanggal()));
            namaViewHolder.textViewTitle.setText("Anda Memasuki Apel Pagi tidak tepat waktu  ");
            String uri = "@drawable/telat";  // where myresource (without the extension) is the file
            int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
            Drawable res = context.getResources().getDrawable(imageResource);
            namaViewHolder.imgIcon.setImageDrawable(res);
        }
        else if(dataKehadiranApelPagi.getStatus().equals("Alpha"))
        {
            namaViewHolder.textViewTanggal.setText(getHari(dataKehadiranApelPagi.getTanggal()));
            namaViewHolder.textViewTitle.setText("Anda Tidak Memasuki Apel");
            String uri = "@drawable/alpha";  // where myresource (without the extension) is the file
            int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
            Drawable res = context.getResources().getDrawable(imageResource);
            namaViewHolder.imgIcon.setImageDrawable(res);
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public  class  NamaViewHolder extends  RecyclerView.ViewHolder{
        ImageView imgIcon;
        TextView textViewTanggal,textViewTitle;
        public NamaViewHolder(@NonNull View itemView) {
            super(itemView);
            imgIcon = (ImageView) itemView.findViewById(R.id.status);
            textViewTanggal = (TextView) itemView.findViewById(R.id.txt_tanggal);
            textViewTitle = (TextView) itemView.findViewById(R.id.nm_kegiatan);
        }
    }

    private List<DataKehadiranApelPagi> data;
    Context context;
    public HistoryApelPagi(List<DataKehadiranApelPagi> data, Context context)
    {
        this.data = data;
        this.context=context;
    }
    String[] Bulan={"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"};
    private String getHari(String tgl)
    {
        int a = -1;
        String bulan = null;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("EEEE");
        Date date;
        try {
            date =simpleDateFormat.parse(tgl);
            calendar.setTime(date);
            a= calendar.get(Calendar.MONTH);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for(int i=0;i<Bulan.length;i++)
        {
            if(i==a)
            {
                bulan=simpleDateFormat2.format(calendar.getTime())+",\n"+calendar.get(Calendar.DATE)+" "+Bulan[i]+" "+calendar.get(Calendar.YEAR);
            }
        }
        if(bulan.isEmpty())
        {
            return tgl;
        }
        else
        {
            return bulan;
        }
    }



}
