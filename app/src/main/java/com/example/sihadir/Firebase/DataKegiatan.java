package com.example.sihadir.Firebase;

public class DataKegiatan {
    String bagian, isi, judul, tanggal, tempat, waktumulai, waktuselesai,kelompok,tujuan, tag;

    String id;

    public DataKegiatan ()
    {

    }

    public String getTag(){
        return tag;
    }
    public void setTag(String tag){
        this.tag=tag;
    }

    public String getBagian() {
        return bagian;
    }

    public void setBagian(String bagian) {
        this.bagian = bagian;
    }

    public String getKelompok() {
        return kelompok;
    }

    public void setKelompok(String kelompok) {
        this.kelompok = kelompok;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id= id;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public String getWaktumulai() {
        return waktumulai;
    }

    public void setWaktumulai(String waktumulai) {
        this.waktumulai = waktumulai;
    }

    public String getWaktuselesai() {
        return waktuselesai;
    }

    public void setWaktuselesai(String waktuselesai) {
        this.waktuselesai = waktuselesai;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan= tujuan;
    }
}
