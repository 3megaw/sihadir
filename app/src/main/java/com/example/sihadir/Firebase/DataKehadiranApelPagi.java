package com.example.sihadir.Firebase;

public class DataKehadiranApelPagi {
    String nip,nama,tanggal,status,waktu_kehadiran;
    int poin;


    public DataKehadiranApelPagi()
    {

    }

    public int getPoin(){
        return poin;
    }
    public void setPoin(int poin){
        this.poin=poin;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getwaktu_kehadiran() {
        return waktu_kehadiran;
    }

    public void setwaktu_kehadiran(String waktu_kehadiran) {
        this.waktu_kehadiran = waktu_kehadiran;
    }


    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
