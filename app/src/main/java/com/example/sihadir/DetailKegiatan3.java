package com.example.sihadir;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.sihadir.Firebase.DataKegiatan;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DetailKegiatan3 extends AppCompatActivity {

    DatabaseReference databaseReference;
    TextView isi,judul,tanggal,tempat,waktumulai,waktuakhir,txttag;

    DataKegiatan dataKegiatan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kegiatan4);

        isi = findViewById(R.id.isi);
        judul = findViewById(R.id.judul);
        tanggal = findViewById(R.id.tanggal);
        tempat = findViewById(R.id.tempat);
        waktumulai = findViewById(R.id.waktu);
        waktuakhir = findViewById(R.id.waktu);
        txttag = findViewById(R.id.tagnama);

        dataKegiatan = new DataKegiatan();

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        final String formattedDate = df.format(c);

        databaseReference = FirebaseDatabase.getInstance().getReference("Kegiatan");
        Query query2 = databaseReference.orderByChild("tanggal").equalTo(formattedDate);
        query2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    dataKegiatan=postSnapshot.getValue(DataKegiatan.class);
                    String gjudul,tgl,tmp,gisi,wkta,wktm,tagnm;
                    gjudul=dataKegiatan.getJudul();
                    tgl=dataKegiatan.getTanggal();
                    gisi=dataKegiatan.getIsi();
                    tmp=dataKegiatan.getTempat();
                    wkta=dataKegiatan.getWaktuselesai();
                    wktm=dataKegiatan.getWaktuselesai();
                    tagnm=dataKegiatan.getTag();

                    judul.setText(gjudul);
                    tanggal.setText(tgl);
                    tempat.setText(tmp);
                    isi.setText(gisi);
                    waktuakhir.setText(wkta);
                    waktumulai.setText(wktm);
                    txttag.setText(tagnm);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
