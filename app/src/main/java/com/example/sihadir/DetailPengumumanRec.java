package com.example.sihadir;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

public class DetailPengumumanRec extends AppCompatActivity {

    TextView txtjudul,txtdivisi,txttanggal,txtjam,txtlokasi,txtdeskripsi,txt5dash;

    DatabaseReference databaseReference;
    DataPengumuman DataPengumuman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pengumuman_rec);

        DataPengumuman=new DataPengumuman();

        txtjudul=findViewById(R.id.judul_detailpengumuman);
        txtdivisi=findViewById(R.id.pengumuman_divisi);
        txttanggal=findViewById(R.id.tanggal);
        txtjam=findViewById(R.id.jam);
        txtlokasi=findViewById(R.id.txtlokasi_pengumuman);
        txtdeskripsi=findViewById(R.id.deskripsi_pengumuman);
        //txt5dash=findViewById(R.id.textView5);

        /*    bundle.putString("key", data1.getTanggal());
                bundle.putString("judul",data1.getJudul());
                bundle.putString("isi",data1.getIsi());
                bundle.putString("waktu",data1.getWaktu());
                bundle.putString("bagian",data1.getWaktu());
                bundle.putString("tempat",data1.getTempat());

         */

        //RECYCLER
        Intent i = this.getIntent();
        String judul=i.getExtras().getString("judul");
        String tgl=i.getExtras().getString("key");
        String isi=i.getExtras().getString("isi");
        String wkt=i.getExtras().getString("waktu");
        String bgn=i.getExtras().getString("bagian");
        String tmp=i.getExtras().getString("tempat");

        txtjudul.setText(judul);
        txtdivisi.setText(bgn);
        txttanggal.setText(tgl);
        txtlokasi.setText(tmp);
        txtjam.setText(wkt);
        txtdeskripsi.setText(isi);
    }
}
