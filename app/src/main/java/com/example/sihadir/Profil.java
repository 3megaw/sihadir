package com.example.sihadir;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.alkathirikhalid.util.Capitalize;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.sihadir.Firebase.DataKegiatan;
import com.example.sihadir.Firebase.DataKehadiranApelPagi;
import com.example.sihadir.Firebase.konek_datadiri;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Profil extends AppCompatActivity {

    TextView buttonData, buttonKegiatan;
    View view1,view2;
    ImageButton imageButton;
    DatabaseReference databaseReference;
    DatabaseReference databaseReference2;
    DatabaseReference databaseReference3;
    DatabaseReference databaseReference4;
    ImageView imageView,btnback,terakhirapelkotakanimgv,terakhirapelkotakanimgv2;
    TextView textViewNama,txtskor, laporanapelpr,trmkasih,bartrmksh;

    String getKeyNip,getKeyDataDiri,keyLogin,keyAndroid, tampungnama,tampungnip;

    konek_login konekLogin;
    konek_datadiri konekDatadiri;
    konek_pengumuman konek_pengumuman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        konekDatadiri=new konek_datadiri();
        konekLogin = new konek_login();

        buttonData = findViewById(R.id.btn_data);
//        buttonKegiatan = findViewById(R.id.btn_Kegiatan);
        imageButton = findViewById(R.id.btn_setting);
        imageView = findViewById(R.id.img);
        textViewNama=findViewById(R.id.nama);
        txtskor=(TextView)findViewById(R.id.txt_SKOR);
        laporanapelpr=findViewById(R.id.laporanapelprofil);
        terakhirapelkotakanimgv=findViewById(R.id.terakhirapelkotakan);
        trmkasih=findViewById(R.id.terimakasih);
        bartrmksh=findViewById(R.id.barterimakasih);
        terakhirapelkotakanimgv2=findViewById(R.id.terakhirapelkotakan2);

        keyAndroid =  Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);

        getKeyLogin();
        getKeyDataDiri();
        kalkhadir();
        setData();
        laporanapel();

        final PopupMenu dropDownMenu = new PopupMenu(Profil.this,imageButton);
        final Menu menu = dropDownMenu.getMenu();
        dropDownMenu.getMenuInflater().inflate(R.menu.option_setting, menu);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dropDownMenu.show();
            }
        });
        imageButton.setOnTouchListener(dropDownMenu.getDragToOpenListener());
        dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId()==R.id.faq)
                {
                    Intent i = new Intent(Profil.this,Tentang.class);
                    startActivity(i);
                }
                return true;
            }
        });

        view1= findViewById(R.id.view1);
//        view2= findViewById(R.id.view2);

        setColorDataAktif();
//        setColorKegiatanNonAktif();
        loadFragment(new DataPeg());
        buttonData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setColorDataAktif();
  //              setColorKegiatanNonAktif();
                Animatoo.animateSlideRight(Profil.this);
                loadFragment(new DataPeg());
            }
        });
        /*
        buttonKegiatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setColorDataNonAktif();
                setColorKegiatanAktif();
                loadFragment(new KehadiranKegiatan());
            }
        });
         */
    }

    public void  getKeyLogin()
    {
        databaseReference = FirebaseDatabase.getInstance().getReference("DataLogin");
        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    konek_login dataLogin = postSnapshot.getValue(konek_login.class);
                    if(dataLogin.getNoAndroid().equals(keyAndroid))
                    {
                        getKeyNip=dataLogin.getNip();
                        keyLogin = postSnapshot.getKey();
                    }
                    else {
            //            Toast.makeText(getApplicationContext(), "KEY LOGIN KOSONG GAN",Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public  void getKeyDataDiri()
    {
        databaseReference = FirebaseDatabase.getInstance().getReference("Data Pegawai");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    konekDatadiri = postSnapshot.getValue(konek_datadiri.class);
                    if (getKeyNip!=null){
                        if(konekDatadiri.getNip().equals(getKeyNip))
                        {
                            getKeyDataDiri=dataSnapshot.getKey();
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Kesalahan Parsing Data, Mohon Tutup Aplikasi dan Membukanya Lagi",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void kalkhadir(){
        databaseReference2 = FirebaseDatabase.getInstance().getReference().child("Kehadiran Apel Pagi");
        databaseReference2.addValueEventListener(new ValueEventListener() {
            final int[] yy = {0};
            String x;
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot zz : dataSnapshot.getChildren())
                {
                    databaseReference3 = databaseReference2.child(zz.getKey());
                    databaseReference3.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            for (DataSnapshot ff : dataSnapshot.getChildren())
                            {
                                DataKehadiranApelPagi dataKehadiranApelPagi6 = ff.getValue(DataKehadiranApelPagi.class);
                                if(dataKehadiranApelPagi6.getNip().equals(getKeyNip) )
                                {
                                    yy[0]=yy[0]+dataKehadiranApelPagi6.getPoin();
                                }
                                //     txtpengumuman.setText(dataKehadiranApelPagi6.getNip());
                            }
                            txtskor.setText(""+yy[0]);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    Date c = Calendar.getInstance().getTime();
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    final String formattedDate = df.format(c);


    private void laporanapel(){
        databaseReference2 = FirebaseDatabase.getInstance().getReference("Kehadiran Apel Pagi");
        databaseReference2.addValueEventListener(new ValueEventListener() {
            String tampungtanggal,tampungwaktu;
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    databaseReference3 = databaseReference2.child(postSnapshot.getKey());
                    databaseReference3.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for(DataSnapshot pS:dataSnapshot.getChildren()){
                                DataKehadiranApelPagi dataKehadiranApelPagi = pS.getValue(DataKehadiranApelPagi.class);
                                if (dataKehadiranApelPagi.getTanggal().equals(formattedDate)) {
                                    if(dataKehadiranApelPagi.getTanggal().equals(getKeyNip)){
                                        tampungtanggal = dataKehadiranApelPagi.getTanggal();
                                        tampungwaktu = dataKehadiranApelPagi.getwaktu_kehadiran();
                                    }
                                    else{
                                        tampungwaktu=null;
                                    }
                                }
                            }
                            if (tampungwaktu==null){
                                trmkasih.setText("Perhatian !");
                                bartrmksh.setText("Hari ini Anda belum presensi apel pagi");
                                laporanapelpr.setText("");
                            }
                            else{
                                terakhirapelkotakanimgv2.setVisibility(View.GONE);
                                trmkasih.setText("Terima Kasih");
                                bartrmksh.setText("Anda telah melakukan presensi apel pagi");
                                laporanapelpr.setText(tampungtanggal+" pukul "+tampungwaktu);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
            }
        }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setData()
    {
        databaseReference = FirebaseDatabase.getInstance().getReference("Data Pegawai");
        databaseReference.addValueEventListener(new ValueEventListener() {
            private Object String;

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    konek_datadiri konekDatadiri = postSnapshot.getValue(konek_datadiri.class);

                    if (postSnapshot.getKey().equals(getKeyNip)){
                        tampungnama = konekDatadiri.getNama();
                        textViewNama.setText(tampungnama);
               //         textViewNama.setText(Capitalize.allFirstLetters(""+tampungnama));
                        if(konekDatadiri.getKelamin().equals("P"))
                        {
                            // TextKeyListener.Capitalize apa = TextKeyListener.Capitalize.valueOf(konekDatadiri.getNama());
                            String uri = "@drawable/female";  // where myresource (without the extension) is the file
                            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                            Drawable res = getResources().getDrawable(imageResource);
                            imageView.setImageDrawable(res);
                        }
                        else if(konekDatadiri.getKelamin().equals("L"))
                        {
                            String uri = "@drawable/male";  // where myresource (without the extension) is the file
                            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                            Drawable res = getResources().getDrawable(imageResource);
                            imageView.setImageDrawable(res);
                        }
                    }
                    }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void  loadFragment(Fragment fragment)
    {
        FragmentManager fragmentManager =getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.frameLayout,fragment);
        fragmentTransaction.commit();
    }

    private void  setColorDataAktif()
    {
        Resources resource =getResources();
        Drawable drawable= ResourcesCompat.getDrawable(resource,R.drawable.btn_biru,null);
        view1.setBackground(drawable);
        buttonData.setTextColor(getResources().getColor(R.color.hitam));
    }
    private void setColorDataNonAktif()
    {
        Resources resource =getResources();
        Drawable drawable= ResourcesCompat.getDrawable(resource,R.color.abuv3,null);
        view1.setBackground(drawable);
        buttonData.setTextColor(getResources().getColor(R.color.abuv2));
    }
    /*
    private void setColorKegiatanAktif()
    {
        Resources resource =getResources();
        Drawable drawable= ResourcesCompat.getDrawable(resource,R.drawable.btn_biru,null);
        view2.setBackground(drawable);
        buttonKegiatan.setTextColor(getResources().getColor(R.color.hitam));
    }
    private void setColorKegiatanNonAktif()
    {
        Resources resource =getResources();
        Drawable drawable= ResourcesCompat.getDrawable(resource,R.color.abuv3,null);
        view2.setBackground(drawable);
        buttonKegiatan.setTextColor(getResources().getColor(R.color.abuv2));
    }

     */
    private String cekKalimat(String nama)
    {
        String c;
        String tp1,tp2="";
        for(int i = 0; i<nama.length();i++)
        {
            c= nama.substring(i);
            if(i==0)
            {
             tp1 = c.toUpperCase();
             tp2+=tp1.toUpperCase();
            }
            else if(nama.substring(i-1).equals(" "))
            {
              tp1=c.toUpperCase();
              tp2+=tp1;
            }
            else
            {
                tp2+=c;
            }
        }
        return tp2;
    }
}
