package com.example.sihadir;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.example.sihadir.Sqlite.SqlLite;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Masuk extends AppCompatActivity {

    EditText edt_npp,edt_pass;
    ImageButton btn_login;
    private ViewFlipper flippertext;

    DatabaseReference databaseReference,databaseReference2;
    FirebaseAuth mAuth;

    konek_login dataLogin;
    konek_datadiri dataDiri;
    String idAdnroid;
    String keyLogin;


    SqlLite sqlLite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_masuk);

        sqlLite = new SqlLite(this);

        mAuth = FirebaseAuth.getInstance();
        dataDiri=new konek_datadiri();
        dataLogin=new konek_login();

        mAuth= FirebaseAuth.getInstance();
        databaseReference= FirebaseDatabase.getInstance().getReference();
        idAdnroid = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);


        edt_npp=(EditText)findViewById(R.id.edt_NPP);
        edt_pass=(EditText)findViewById(R.id.edt_password);
        btn_login=(ImageButton)findViewById(R.id.btn_login);
        edt_pass.setFilters(new InputFilter[]{new InputFilter.AllCaps()});


        flippertext = (ViewFlipper) findViewById(R.id.vflip);

        Animation in = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);

        flippertext.setInAnimation(in);
        flippertext.setOutAnimation(out);
       flippertext.setFlipInterval(6000);
       flippertext.setAutoStart(true);
/*
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Query query = databaseReference.child("Data Pegawai").orderByChild("nip").equalTo(edt_npp.getText().toString().trim());
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            konek_login konekLogin = postSnapshot.getValue(konek_login.class);
                            if (konekLogin.nama.equals(edt_pass.getText().toString().trim())) {
                                Toast.makeText(Masuk.this, "success", Toast.LENGTH_SHORT).show();
                                Intent i=new Intent(Masuk.this, Menu.class);
                                startActivity(i);
                            } else {
                                Toast.makeText(Masuk.this, "wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });


                    /*    if (TextUtils.isEmpty(edt_npp.getText().toString().trim())){
                    edt_npp.setError("NIP Tidak Boleh Kosong !");
                }
                if (TextUtils.isEmpty(edt_pass.getText().toString().trim())){
                    edt_pass.setError("Nama TIDAK BOLEH KOSONG !");
                }

                else{
                    Intent i=new Intent(Login.this, Menu.class);
                    startActivity(i);
                } */

    }


    public void cekLogin()
    {
        Query query = databaseReference.child("Data Pegawai").orderByChild("nip").equalTo(edt_npp.getText().toString().trim());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int a=0;
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    dataDiri = postSnapshot.getValue(konek_datadiri.class);
                    if(dataDiri.getNama().equals(edt_pass.getText().toString().trim()))
                    {
                        a=1;
                    }
                }
                if(a==1)
                {
                    cekAkunLogin();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
    public  void cekAkunLogin()
    {
        databaseReference = FirebaseDatabase.getInstance().getReference("DataLogin");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int a=0;
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    konek_login dataLogin1 = postSnapshot.getValue(konek_login.class);
                    if(dataLogin1.getNip().equals(edt_npp.getText().toString()))
                    {
                        if (dataLogin1.getNoAndroid().equals(idAdnroid)){
                            a=1;
                            keyLogin = postSnapshot.getKey();}
                        else{
                            a=2;
                            keyLogin=postSnapshot.getKey();
                        }
                    }
                }
                if(a==2)
                {
                    AlertDialog.Builder builder=new AlertDialog.Builder(Masuk.this);
                    builder.setTitle("Pemberitahuan");
                    builder.setMessage("Akun ini sudah digunakan di perangkat lain, jika ingin melanjutkan maka data perangkat sebelumnya akan hilang. lanjutkan ?")
                            .setCancelable(false)
                            .setPositiveButton("Iya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    onClick2(keyLogin);
                                    // Toast.makeText(getApplicationContext(),"Mengirim data",Toast.LENGTH_LONG).show();
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Toast.makeText(getApplicationContext(),"Gunakan dengan bijak",Toast.LENGTH_LONG).show();
                                    dialogInterface.cancel();
                                }
                            });
                    AlertDialog alertDialog=builder.create();
                    alertDialog.show();
                }
                else if(a==1){
                    Intent intent = new Intent(Masuk.this,Menu.class);
                    startActivity(intent);
                }
                else
                {
                    saveLogin();
                    Intent intent = new Intent(Masuk.this,Menu.class);
                    startActivity(intent);
                    finish();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public  void  updateDataLogin(String key)
    {
        databaseReference2 = FirebaseDatabase.getInstance().getReference().child("DataLogin");
        dataLogin.setNoAndroid(idAdnroid);
        dataLogin.setNip(edt_npp.getText().toString());
        databaseReference2.child(keyLogin).setValue(dataLogin).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //saveLogin();
                Intent intent = new Intent(Masuk.this,Menu.class);
                startActivity(intent);
                // Animatoo.animateZoom(Login.this);

            }
        });
    }
    public void saveLogin()
    {
        databaseReference2 = FirebaseDatabase.getInstance().getReference().child("DataLogin");
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Data Pegawai");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    dataDiri = postSnapshot.getValue(konek_datadiri.class);
                    if(dataDiri.getNip().equals(edt_npp.getText().toString().trim()))
                    {
                        dataLogin.setNip(dataDiri.getNip());
                        dataLogin.setNoAndroid(idAdnroid);
                        dataLogin.setNama(dataDiri.getNama());
                        databaseReference2.push().setValue(dataLogin);

                    }

                    boolean isInserted = sqlLite.insertData(edt_npp.getText().toString(),edt_pass.getText().toString().trim());
                    if (isInserted == true){
                        //     Toast.makeText(Login.this, "Data Tersimpan", Toast.LENGTH_LONG).show();
                        //   break;
                    }
                    else {
                        //        Toast.makeText(Login.this, "Data Gagal Tersimpan", Toast.LENGTH_LONG).show();
                        //     break;
                    }

                }finish();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public void onClick2(final String key) {
        /*menampilkan dialog*/
        final ProgressDialog dialog = ProgressDialog.show(
                this, "Sedang memproses data", "Silahkan tunggu...", true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    /*mensimulasikan sesuatu proses yg lama*/
                    Thread.sleep(5000);
                    updateDataLogin(key);
                    /*hentikan dialog*/
                    dialog.dismiss();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public void login(View view)
    {
        int a= edt_npp.getText().toString().length();
        if(TextUtils.isEmpty(edt_npp.getText().toString().trim()))
        {
            edt_npp.setError("NIP tidak boleh kosong !");
            // textViewAlert.setText("Nomer telepon tidak boleh kosong");
        }
        else if(a>18)
        {
            edt_npp.setError("NIP lebih dari 18 digit !");
            //Toast.makeText(this, "Nomer Telepon lebih dari 12/13 digit", Toast.LENGTH_SHORT).show();
        }
        else if(a<18)
        {
            edt_npp.setError("NIP kurang dari 18 digit !");
            //Toast.makeText(this, "Nomer Telepon kurang dari 12/13 digit", Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(edt_pass.getText().toString().trim())){
            edt_pass.setError("Nama tidak boleh kosong !");
        }
        else
        {
            cekLogin();
        }

    }

}
