package com.example.sihadir;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.sihadir.Firebase.DataKegiatan;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Pengumuman extends AppCompatActivity {

    private List<DataPengumuman> data = new ArrayList<>();
    private DatabaseReference databaseReference;
    RecyclerView recyclerView;
    ListDaftarPengumuman listDaftarPengumuman;

    TextView txttgl,txtjudul,txtisi,txtlihatsemuapengumuman;
    DataPengumuman dataPengumuman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengumuman);

        txttgl= findViewById(R.id.tgl);
        txtjudul= findViewById(R.id.judul);
        txtisi= findViewById(R.id.isi);
        txtlihatsemuapengumuman=findViewById(R.id.lihatsemuapengumuman);

        txtlihatsemuapengumuman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Pengumuman.this,PengumumanBanyak.class);
                startActivity(i);
            }
        });

        ///PENGUMUMAN TERBARU
        databaseReference = FirebaseDatabase.getInstance().getReference("Pengumuman");
        Query query = databaseReference.orderByKey().limitToLast(1);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    dataPengumuman  = postSnapshot.getValue(DataPengumuman.class);
                    String tampungjudul,tampungtanggal,tampungisi;

                    tampungtanggal = dataPengumuman.getTanggal();
                    tampungisi = dataPengumuman.getIsi();
                    tampungjudul = dataPengumuman.getJudul();

                    txtjudul.setText(tampungjudul);
                    txttgl.setText(tampungtanggal);
                    txtisi.setText(tampungisi);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        txtjudul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Pengumuman.this,DetailPengumuman.class);
                startActivity(i);
            }
        });

        //LOAD DATA RECYCLER
        onDaftar();
    }
    private void onDaftar(){
        recyclerView = findViewById(R.id.viewdaftar);
        listDaftarPengumuman = new ListDaftarPengumuman(this,data);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(listDaftarPengumuman);

        databaseReference = FirebaseDatabase.getInstance().getReference("Pengumuman");
        Query query = databaseReference.orderByKey().limitToLast(5);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    DataPengumuman dape =postSnapshot.getValue(DataPengumuman.class);
                    data.add(dape);
                    Collections.reverse(data);
                }

                listDaftarPengumuman.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
/*
    public void backbt(View view){
        Intent i = new Intent(this,pengumuman.class);
        startActivity(i);
    }
 */
}
