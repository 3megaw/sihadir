package com.example.sihadir;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.sihadir.Firebase.Konek_Upacara;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DetailUpc3 extends AppCompatActivity {

    DatabaseReference databaseReference;
    TextView id,isi,judul,tanggal,tempat,waktu,baju;

    Konek_Upacara konek_upacara;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_upc3);

        konek_upacara = new Konek_Upacara();

        isi = findViewById(R.id.txtisi);
        judul = findViewById(R.id.judul_detailupacara);
        tanggal = findViewById(R.id.txtcalendar);
        tempat = findViewById(R.id.txtlocation);
        waktu = findViewById(R.id.txtclock);
        baju= findViewById(R.id.txtuniform);


        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        final String formattedDate = df.format(c);

        databaseReference = FirebaseDatabase.getInstance().getReference("Upacara");
        Query query2 = databaseReference.orderByChild("Tanggal").equalTo(formattedDate);
        query2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    konek_upacara=postSnapshot.getValue(Konek_Upacara.class);
                    String gjudul,tgl,tmp,gisi,bj,wktm;
                    gjudul=konek_upacara.getJudul();
                    tgl=konek_upacara.getTanggal();
                    gisi=konek_upacara.getIsi();
                    tmp=konek_upacara.getLokasi();
                    bj=konek_upacara.getBaju();
                    wktm=konek_upacara.getWaktu();

                    judul.setText(gjudul);
                    tanggal.setText(tgl);
                    tempat.setText(tmp);
                    isi.setText(gisi);
                    baju.setText(bj);
                    waktu.setText(wktm);
            }
             }
            //}
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
