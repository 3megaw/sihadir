package com.example.sihadir;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;

public class DetailUpc extends AppCompatActivity {

    DatabaseReference databaseReference;
    TextView id,isi,judul,tanggal,tempat,waktu,baju;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_upc);

        isi = findViewById(R.id.txtisi);
        judul = findViewById(R.id.judul_detailupacara);
        tanggal = findViewById(R.id.txtcalendar);
        tempat = findViewById(R.id.txtlocation);
        waktu = findViewById(R.id.txtclock);
        baju= findViewById(R.id.txtuniform);

        //GET DATA
        Intent i = this.getIntent();
        String gjudul=i.getExtras().getString("judul");
        String tgl=i.getExtras().getString("tanggal");
        String kode=i.getExtras().getString("key");
        String gisi=i.getExtras().getString("isi");
        String wktm=i.getExtras().getString("waktu");
        String tmp=i.getExtras().getString("lokasi");
        String bj=i.getExtras().getString("baju");

        judul.setText(gjudul);
        tanggal.setText(tgl);
        tempat.setText(tmp);
        isi.setText(gisi);
        baju.setText(bj);
        waktu.setText(wktm);
    }
}
