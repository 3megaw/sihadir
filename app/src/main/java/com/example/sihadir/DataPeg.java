package com.example.sihadir;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.provider.Settings.Secure;

import com.alkathirikhalid.util.Capitalize;
import com.example.sihadir.Firebase.konek_datadiri;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class DataPeg extends Fragment {
    View view;
    DatabaseReference databaseReference;
    TextView nip,nama,agama,alamat,eselon,gol,jabatan,kelamin,telepon,tgllahir,tmtgol,tmtjab,tplahir,unit1,unit2,unit3;


    konek_login konekLogin;
    konek_datadiri konekDatadiri;
    konek_pengumuman konek_pengumuman;

    String getKeyNip,getKeyDataDiri,keyLogin,keyAndroid, tampungnama,tampungnip;


    ProgressBar pb;
    public  View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState)
    {

        keyAndroid =  Settings.Secure.getString(getContext().getContentResolver(),Settings.Secure.ANDROID_ID);

        Profil profilhehe = new Profil();
        konekDatadiri=new konek_datadiri();
        konekLogin = new konek_login();



        view=  inflater.inflate(R.layout.data_pegawai,container,false);
        nama = view.findViewById(R.id.txt_nama);
        nip = view.findViewById(R.id.txt_nip);
        agama = view.findViewById(R.id.txt_agama);
        alamat = view.findViewById(R.id.txt_alamat);
        eselon = view.findViewById(R.id.txt_aselon);
        gol = view.findViewById(R.id.txt_gol);
        jabatan = view.findViewById(R.id.txt_jabatan);
        kelamin = view.findViewById(R.id.txt_gender);
        telepon = view.findViewById(R.id.txt_tel);
        tgllahir = view.findViewById(R.id.txt_ttl);
        unit1 = view.findViewById(R.id.txt_unit1);
        unit2 = view.findViewById(R.id.txt_unit2);
        unit3 = view.findViewById(R.id.txt_unit3);
        //pb = view.findViewById(R.id.muter);
        //FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        //databaseReference= FirebaseDatabase.getInstance().getReference("Data Pegawai");
        //databaseReference.keepSynced(true);

        getKeyLogin();
        getKeyDataDiri();
        setData();
        //new MyTask();

        return  view;
    }



    public void  getKeyLogin()
    {
        databaseReference = FirebaseDatabase.getInstance().getReference("DataLogin");
        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    konek_login dataLogin = postSnapshot.getValue(konek_login.class);
                    if(dataLogin.getNoAndroid().equals(keyAndroid))
                    {
                        getKeyNip=dataLogin.getNip();
                        keyLogin = postSnapshot.getKey();

                    }
                    else {
                        //            Toast.makeText(getApplicationContext(), "KEY LOGIN KOSONG GAN",Toast.LENGTH_LONG).show();
                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    public  void getKeyDataDiri()
    {
        databaseReference = FirebaseDatabase.getInstance().getReference("Data Pegawai");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    konekDatadiri = postSnapshot.getValue(konek_datadiri.class);
                    if (getKeyNip!=null){
                        if(konekDatadiri.getNip().equals(getKeyNip))
                        {
                            getKeyDataDiri=dataSnapshot.getKey();
                        }
                    }
                    else{
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    private void setData()
    {
        databaseReference = FirebaseDatabase.getInstance().getReference("Data Pegawai");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    com.example.sihadir.Firebase.konek_datadiri konekDatadiri = postSnapshot.getValue(konek_datadiri.class);
                    if(postSnapshot.getKey().equals(getKeyNip))
                    {
                        if(konekDatadiri.getKelamin().equals("L"))
                        {
                            nip.setText(konekDatadiri.getNip());
                            nama.setText(konekDatadiri.getNama());
                            agama.setText(konekDatadiri.getAgama());
                            alamat.setText(konekDatadiri.getAlamat());
                            eselon.setText(konekDatadiri.getEselon());
                            gol.setText(konekDatadiri.getGol());
                            jabatan.setText(konekDatadiri.getJabatan());
                            kelamin.setText("Laki-Laki");
                            telepon.setText(konekDatadiri.getTelepon());
                            tgllahir.setText(konekDatadiri.getTgllahir());
                            unit1.setText(konekDatadiri.getUnit1());
                            unit2.setText(konekDatadiri.getUnit2());
                            unit3.setText(konekDatadiri.getUnit3());
                        }
                        else if(konekDatadiri.getKelamin().equals("P"))
                        {
                            nip.setText(konekDatadiri.getNip());
                            nama.setText(Capitalize.allFirstLetters(konekDatadiri.getNama()));
                            agama.setText(konekDatadiri.getAgama());
                            alamat.setText(konekDatadiri.getAlamat());
                            eselon.setText(konekDatadiri.getEselon());
                            gol.setText(konekDatadiri.getGol());
                            jabatan.setText(konekDatadiri.getJabatan());
                            kelamin.setText("Perempuan");
                            telepon.setText(konekDatadiri.getTelepon());
                            tgllahir.setText(konekDatadiri.getTgllahir());
                            unit1.setText(konekDatadiri.getUnit1());
                            unit2.setText(konekDatadiri.getUnit2());
                            unit3.setText(konekDatadiri.getUnit3());
                        }
                        else
                        {
                            nip.setText(konekDatadiri.getNip());
                            nama.setText(Capitalize.allFirstLetters(konekDatadiri.getNama()));
                            agama.setText(konekDatadiri.getAgama());
                            alamat.setText(konekDatadiri.getAlamat());
                            eselon.setText(konekDatadiri.getEselon());
                            gol.setText(konekDatadiri.getGol());
                            jabatan.setText(konekDatadiri.getJabatan());
                            kelamin.setText(konekDatadiri.getKelamin());
                            telepon.setText(konekDatadiri.getTelepon());
                            tgllahir.setText(konekDatadiri.getTgllahir());
                            unit1.setText(konekDatadiri.getUnit1());
                            unit2.setText(konekDatadiri.getUnit2());
                            unit3.setText(konekDatadiri.getUnit3());
                        }
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private class MyTask extends AsyncTask<String,String,String>
    {

        @Override
        protected void onPreExecute() {

            pb.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... params) {
            databaseReference.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                    {
                        com.example.sihadir.Firebase.konek_datadiri konekDatadiri = postSnapshot.getValue(konek_datadiri.class);
                        if(postSnapshot.getKey().equals(getKeyNip))
                        {
                            nip.setText(konekDatadiri.getNip());
                            nama.setText(konekDatadiri.getNama());
                            agama.setText(konekDatadiri.getAgama());
                        /*alamat.setText(konekDatadiri.getAlamat());
                        eselon.setText(konekDatadiri.getEselon());
                        gol.setText(konekDatadiri.getGol());
                        jabatan.setText(konekDatadiri.getJabatan());
                        kelamin.setText(konekDatadiri.getKelamin());
                        telepon.setText(konekDatadiri.getTelepon());
                        tgllahir.setText(konekDatadiri.getTgllahir());
                        unit1.setText(konekDatadiri.getUnit1());
                        unit2.setText(konekDatadiri.getUnit2());
                        unit3.setText(konekDatadiri.getUnit3());*/
                        }
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) { }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) { }
                @Override
                public void onCancelled(DatabaseError databaseError) { }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pb.setVisibility(View.INVISIBLE);
            super.onPostExecute(s);
        }
    }
}
