package com.example.sihadir;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sihadir.Firebase.Konek_Upacara;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ListUpacara extends RecyclerView.Adapter<ListUpacara.NamaViewHolder> {

    @NonNull
    @Override
    public ListUpacara.NamaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.list_upc,viewGroup,false);
        return new ListUpacara.NamaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListUpacara.NamaViewHolder namaViewHolder, final int i) {

        final Konek_Upacara konek_upacara = data.get(i);
        namaViewHolder.judulU.setText(konek_upacara.getJudul());
        namaViewHolder.tanggalU.setText(konek_upacara.getTanggal());
        namaViewHolder.isiU.setText(konek_upacara.getIsi());

        namaViewHolder.judulU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("key", konek_upacara.getId());
                bundle.putString("judul",konek_upacara.getJudul());
                bundle.putString("isi",konek_upacara.getIsi());
                bundle.putString("lokasi",konek_upacara.getLokasi());
                bundle.putString("waktu",konek_upacara.getWaktu());
                bundle.putString("baju",konek_upacara.getBaju());
                bundle.putString("tanggal",konek_upacara.getTanggal());

                Intent intent = new Intent(context,DetailUpc.class);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public  class  NamaViewHolder extends  RecyclerView.ViewHolder{

        TextView judulU;
        TextView tanggalU;
        TextView isiU;
        public NamaViewHolder(@NonNull View itemView) {
            super(itemView);

            tanggalU = (TextView) itemView.findViewById(R.id.tgl);
            judulU = (TextView) itemView.findViewById(R.id.judul);
            isiU = (TextView) itemView.findViewById(R.id.isi);
        }
    }

    private List<Konek_Upacara> data;
    Context context;
    String[] Bulan={"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"};
    public ListUpacara(List<Konek_Upacara> data, Context context)
    {
        this.data = data;
        this.context=context;
    }
    /*
    private String getHari(String tgl)
    {
        int a = -1;
        String bulan = null;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("EEEE");
        Date date;
        try {
            date =simpleDateFormat.parse(tgl);
            calendar.setTime(date);
            a= calendar.get(Calendar.MONTH);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for(int i=0;i<Bulan.length;i++)
        {
            if(i==a)
            {
                bulan=simpleDateFormat2.format(calendar.getTime())+", "+calendar.get(Calendar.DATE)+" "+Bulan[i]+" "+calendar.get(Calendar.YEAR);
            }
        }
        if(bulan.isEmpty())
        {
            return tgl;
        }
        else
        {

            return bulan;
        }
    }

     */

}
