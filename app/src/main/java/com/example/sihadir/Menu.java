package com.example.sihadir;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alkathirikhalid.util.Capitalize;
import com.example.sihadir.Firebase.DataKehadiranApelPagi;
import com.example.sihadir.Firebase.Konek_Upacara;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.WriterException;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class Menu extends AppCompatActivity {

    TextView banyak_pengumuman,txtdashboard_nama, txtpengumumanjudul,txtpengumumanwaktu,txtpengumumanisi,txtpengumuman,txtprofil,txtpengumumandetail,txtbanyakpengumuman;
    ImageView qrcode,menukegiatan,menuupacara,laypengumuman,imageView_gender;

    //TEST
    TextView persentase;

    EditText tampung;

    konek_login konekLogin;
    konek_datadiri konekDatadiri;
    konek_pengumuman konek_pengumuman;
    DataPengumuman DataPengumuman;
    Konek_Upacara konek_upacara;
    DataKehadiranApelPagi dataKehadiranApelPagi;

    cek_konek_tampung cek_konek_tampung;

    String keyAndroid;
    String TAG="GenerateQrCode", inputvalue;
    String getKeyNip,getKeyDataDiri,keyLogin, tampungnama,firstWord,tampungjam;

    Bitmap bitmap;
    QRGEncoder qrgEncoder;

    DatabaseReference databaseReference;
    DatabaseReference databaseReference2;
    DatabaseReference databaseReference3;
    DatabaseReference databaseReference4;

    String tampungnipgan,tampungnipgan2;
    Long poin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        konek_datadiri konekDatadiri;
        konekLogin = new konek_login();
        konek_pengumuman = new konek_pengumuman();
        cek_konek_tampung = new cek_konek_tampung();
        DataPengumuman=new DataPengumuman();



        menukegiatan = findViewById(R.id.menu_kegiatan);
        menuupacara = findViewById(R.id.menu_upacara);
        laypengumuman=findViewById(R.id.corner_pengumuman);
        imageView_gender=findViewById(R.id.imageView4);
        banyak_pengumuman=findViewById(R.id.banyak_pengumuman);

        txtpengumumandetail=findViewById(R.id.pengumuman_lihat);
        txtpengumuman = findViewById(R.id.Pengumuman);
        txtdashboard_nama=findViewById(R.id.nama_dashboard);
        txtpengumumanjudul=findViewById(R.id.pengumuman_judul);
        txtpengumumanwaktu=findViewById(R.id.pengumuman_waktu);
        txtpengumumanisi=findViewById(R.id.pengumuman_detail);
        txtbanyakpengumuman=findViewById(R.id.banyak_pengumuman);
        txtprofil=findViewById(R.id.LihatProfil);
        persentase=findViewById(R.id.persentase_dashboard);
        qrcode=findViewById(R.id.qr_code);
        tampung=findViewById(R.id.tampung);


        //POIN POIN POIN POIN POIN POIN POIN WOI POIN POIN 1!!!!11111!1!1!!!!!!!
        //cek
        //TES

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-M-dd");
        Date dateStr = null;
/*
        try {
           dateStr = df.parse("2020-2-18");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = df.format(dateStr);
*/
        databaseReference2 = FirebaseDatabase.getInstance().getReference().child("Kehadiran Apel Pagi");
           databaseReference2.addValueEventListener(new ValueEventListener() {
               final int[] yy = {0};
               String x;
               @Override
               public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for(DataSnapshot zz : dataSnapshot.getChildren())
                    {
                        databaseReference3 = databaseReference2.child(zz.getKey());
                        databaseReference3.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                for (DataSnapshot ff : dataSnapshot.getChildren())
                                {
                                    DataKehadiranApelPagi dataKehadiranApelPagi6 = ff.getValue(DataKehadiranApelPagi.class);
                                    if(dataKehadiranApelPagi6.getNip().equals(getKeyNip) )
                                    {
                                        yy[0]=yy[0]+dataKehadiranApelPagi6.getPoin();
                                    }
                               //     txtpengumuman.setText(dataKehadiranApelPagi6.getNip());
                                }
                                persentase.setText(""+yy[0]);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
               }

               @Override
               public void onCancelled(@NonNull DatabaseError databaseError) {

               }
           });
//        txtpengumuman.setText(formattedDate);

        /*
        databaseReference2 = FirebaseDatabase.getInstance().getReference("Kehadiran Apel Pagi");
        databaseReference3 = (DatabaseReference) databaseReference2.child("2020-2-18");
         Query query2 = databaseReference3.child("196006061984031001");
        // Query query3 = ((DatabaseReference) query2).child("status").equalTo("Hadir");

        query2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                   dataKehadiranApelPagi = postSnapshot.getValue(DataKehadiranApelPagi.class);
                    int nam = dataKehadiranApelPagi.getPoin();
                    //    poin = poin+tpoin;
                    txtpengumuman.setText(nam);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
         */
        laypengumuman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                 */
                Intent i = new Intent(Menu.this, DetailPengumuman.class);
                startActivity(i);
            }
        });

        banyak_pengumuman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Menu.this, Pengumuman.class);
                startActivity(i);
            }
        });

        menuupacara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Menu.this, Upacara.class);
                startActivity(i);
            }
        });

        menukegiatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Menu.this,Kegiatan.class);
                startActivity(i);
            }
        });

        txtprofil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Menu.this,Profil.class);
                startActivity(i);
            }
        });
        imageView_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Menu.this,Profil.class);
                startActivity(i);
            }
        });



        keyAndroid= Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);

        getKeyLogin();
        getKeyDataDiri();
        setData();
//        setqr();

        databaseReference = FirebaseDatabase.getInstance().getReference("Pengumuman");
        Query query = databaseReference.orderByKey().limitToLast(1);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    konek_pengumuman  = postSnapshot.getValue(konek_pengumuman.class);
                    String tampungjudul,tampungwaktu,tampungisi;

                    tampungjam=konek_pengumuman.getWaktu();
                    tampungwaktu = konek_pengumuman.getTanggal();
                    tampungisi = konek_pengumuman.getIsi();
                    tampungjudul = konek_pengumuman.getJudul();

                    txtpengumumanjudul.setText(tampungjudul);
                    txtpengumumanwaktu.setText(tampungwaktu+" Pukul "+tampungjam+" WIB");
                    txtpengumumanisi.setText(tampungisi);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        tampungnipgan2=tampungnipgan;
/*
       databaseReference2 = FirebaseDatabase.getInstance().getReference(""+tampungnipgan2);
//               .child(""+getKeyNip)
  //             .child("file");
       databaseReference2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    String link=dataSnapshot.getValue(String.class);
                    Picasso.get().load(link).into(qrcode);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
 */
   }
    public void  getKeyLogin()
    {
        databaseReference = FirebaseDatabase.getInstance().getReference("DataLogin");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    konek_login dataLogin = postSnapshot.getValue(konek_login.class);
                    if(dataLogin.getNoAndroid().equals(keyAndroid))
                    {
                        getKeyNip=dataLogin.getNip();
                        keyLogin = postSnapshot.getKey();

                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public  void getKeyDataDiri()
    {
     databaseReference = FirebaseDatabase.getInstance().getReference("Data Pegawai");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
               {
                    konekDatadiri = postSnapshot.getValue(konek_datadiri.class);
                    if (getKeyNip!=null){
                        if(konekDatadiri.getNip().equals(getKeyNip))
                        {
                            getKeyDataDiri=dataSnapshot.getKey();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void setData()
    {
        databaseReference = FirebaseDatabase.getInstance().getReference("Data Pegawai");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    konekDatadiri= postSnapshot.getValue(konek_datadiri.class);
                    cek_konek_tampung=postSnapshot.getValue(cek_konek_tampung.class);
                    if(konekDatadiri.getNip().equals(getKeyNip))
                    {
                        tampungnama = konekDatadiri.getNama();
                        tampungnipgan = konekDatadiri.getNip();
                        tampungnipgan2=tampungnipgan;
                        tampung.setText(tampungnipgan2);
                        inputvalue=tampung.getText().toString().trim();



                        if(konekDatadiri.getKelamin().equals("P"))
                        {
                            // TextKeyListener.Capitalize apa = TextKeyListener.Capitalize.valueOf(konekDatadiri.getNama());
                            String uri = "@drawable/female";  // where myresource (without the extension) is the file
                            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                            Drawable res = getResources().getDrawable(imageResource);
                            imageView_gender.setImageDrawable(res);
                        }
                        else if(konekDatadiri.getKelamin().equals("L"))
                        {
                            String uri = "@drawable/male";  // where myresource (without the extension) is the file
                            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                            Drawable res = getResources().getDrawable(imageResource);
                            imageView_gender.setImageDrawable(res);
                        }
                        if(inputvalue.length()>0){
                            WindowManager manager=(WindowManager)getSystemService(WINDOW_SERVICE);
                            Display display=manager.getDefaultDisplay();
                            Point point=new Point();
                            display.getSize(point);
                            int width=point.x;
                            int height = point.y;
                            int smallerdimension=width>height ? width:height;
                            smallerdimension=smallerdimension*1;
                            qrgEncoder=new QRGEncoder(inputvalue,null, QRGContents.Type.TEXT,smallerdimension);
                            try {
                                bitmap=qrgEncoder.encodeAsBitmap();
                                qrcode.setImageBitmap(bitmap);
                            } catch (WriterException e) {
                                Log.v(TAG,e.toString());
                            }
                        }else{
                            tampung.setError("Erro");
                        }
                        Capitalize.allFirstLetters(""+tampungnama);
                        if(tampungnama.contains(" ")){
                            String arr[] = tampungnama.split(" ",3);
                            firstWord= arr[0];
                            txtdashboard_nama.setText(firstWord);
                            txtdashboard_nama.setText(Capitalize.allFirstLetters(""+firstWord));
                        }else{
                            txtdashboard_nama.setText(Capitalize.allFirstLetters(""+tampungnama));
                        }

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    public void setqr(){


    }

}