package com.example.sihadir;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sihadir.Firebase.DataKegiatan;
import com.example.sihadir.Firebase.Konek_Upacara;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class Kegiatan extends AppCompatActivity {


    private List<DataKegiatan> data_Kegiatan = new ArrayList<>();
    private DatabaseReference databaseReference;
    DataKegiatan dataKegiatan;

    RecyclerView recyclerView;
    ListKegiatan listKegiatan;
//    ImageView imgback;
    Button buttonUmum,buttonDivisi,buttonAtasan;

    TextView txttgl,txtjudul,txtisi,txttgl2,txtjudul2,txtisi2;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegiatan);
      //  buttonUmum = findViewById(R.id.button1);
       // buttonDivisi = findViewById(R.id.button2);
       // buttonAtasan= findViewById(R.id.button3);
      //  imgback = findViewById(R.id.back);

        txttgl= findViewById(R.id.tgl);
        txtjudul= findViewById(R.id.judul);
        txtisi= findViewById(R.id.isi);
        txttgl2= findViewById(R.id.tgl2);
        txtjudul2= findViewById(R.id.judul2);
        txtisi2= findViewById(R.id.isi2);


        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        final String formattedDate = df.format(c);


        ///KEGIATAN TERBARU
        databaseReference = FirebaseDatabase.getInstance().getReference("Kegiatan");
        Query query = databaseReference.orderByKey().limitToLast(1);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    dataKegiatan  = postSnapshot.getValue(DataKegiatan.class);
                    String tampungjudul,tampungtanggal,tampungisi;

                    tampungtanggal = dataKegiatan.getTanggal();
                    tampungisi = dataKegiatan.getIsi();
                    tampungjudul = dataKegiatan.getJudul();

                    txtjudul.setText(tampungjudul);
                    txttgl.setText(tampungtanggal);
                    txtisi.setText(tampungisi);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        ///KEGIATAN BERLANGSUNG
        databaseReference = FirebaseDatabase.getInstance().getReference("Kegiatan");
        Query query2 = databaseReference.orderByChild("tanggal").equalTo(formattedDate);
        query2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    dataKegiatan  = postSnapshot.getValue(DataKegiatan.class);
                    String tampungjudul,tampungtanggal,tampungisi;
                    tampungtanggal = dataKegiatan.getTanggal();
                    tampungisi = dataKegiatan.getIsi();
                    tampungjudul = dataKegiatan.getJudul();

                    //     if (tampungtanggal.equals(formattedDate)){
                    txtjudul2.setText(tampungjudul);
                    txttgl2.setText(tampungtanggal);
                    txtisi2.setText(tampungisi);}
                //   else{
            }
            // }
            //}
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        txtjudul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Kegiatan.this,DetailKegiatan2.class);
                startActivity(i);
            }
        });

        txtjudul2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Kegiatan.this,DetailKegiatan3.class);
                startActivity(i);
            }
        });

        umum();
/*
        buttonUmum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                umum();
            }
        });
        buttonDivisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                divisi();
            }
        });
        buttonAtasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                atasan();
            }
        });
/*
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Kegiatan.this, Menu.class);
                startActivity(i);
            }
        });
 */
    }

    private void umum()
    {
        recyclerView = findViewById(R.id.DaftarKegiatan);
        listKegiatan = new ListKegiatan(data_Kegiatan,Kegiatan.this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(listKegiatan);

    /*    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date date = new Date();
     */
        databaseReference = FirebaseDatabase.getInstance().getReference();
        Query query= databaseReference.child("Kegiatan").orderByChild("tanggal");//.startAt(dateFormat.format(date));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data_Kegiatan.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    DataKegiatan kegiatan = postSnapshot.getValue(DataKegiatan.class);
                    data_Kegiatan.add(kegiatan);
                    Collections.reverse(data_Kegiatan);
                }
                listKegiatan.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void divisi()
    {
        recyclerView = findViewById(R.id.DaftarKegiatan);
        listKegiatan = new ListKegiatan(data_Kegiatan,Kegiatan.this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(listKegiatan);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        Query query= databaseReference.child("Kegiatan").orderByChild("tanggal");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data_Kegiatan.clear();

                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    DataKegiatan kegiatan = postSnapshot.getValue(DataKegiatan.class);
                    if(kegiatan.getKelompok().equals("divisi") && kegiatan.getBagian().equals("Humas")) {

                        data_Kegiatan.add(kegiatan);
                        Collections.reverse(data_Kegiatan);
                    }
                }
                listKegiatan.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void atasan ()
    {
        recyclerView = findViewById(R.id.DaftarKegiatan);
        listKegiatan = new ListKegiatan(data_Kegiatan,Kegiatan.this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(listKegiatan);


        databaseReference = FirebaseDatabase.getInstance().getReference();
        Query query= databaseReference.child("Kegiatan").orderByChild("tanggal");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data_Kegiatan.clear();

                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    DataKegiatan kegiatan = postSnapshot.getValue(DataKegiatan.class);
                    if(kegiatan.getKelompok().equals("Kepala")) {

                        data_Kegiatan.add(kegiatan);
                        Collections.reverse(data_Kegiatan);
                    }
                }
                listKegiatan.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
