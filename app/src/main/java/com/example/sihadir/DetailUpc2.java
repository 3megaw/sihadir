package com.example.sihadir;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.sihadir.Firebase.Konek_Upacara;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class DetailUpc2 extends AppCompatActivity {

    DatabaseReference databaseReference;
    TextView id,isi,judul,tanggal,tempat,waktu,baju;

    Konek_Upacara konek_upacara;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_upc2);

        konek_upacara = new Konek_Upacara();

        isi = findViewById(R.id.txtisi);
        judul = findViewById(R.id.judul_detailupacara);
        tanggal = findViewById(R.id.txtcalendar);
        tempat = findViewById(R.id.txtlocation);
        waktu = findViewById(R.id.txtclock);
        baju= findViewById(R.id.txtuniform);

        databaseReference = FirebaseDatabase.getInstance().getReference("Upacara");
        Query query = databaseReference.orderByKey().limitToLast(1);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    konek_upacara=postSnapshot.getValue(Konek_Upacara.class);
                    String gjudul,tgl,tmp,gisi,bj,wktm;
                    gjudul=konek_upacara.getJudul();
                    tgl=konek_upacara.getTanggal();
                    gisi=konek_upacara.getIsi();
                    tmp=konek_upacara.getLokasi();
                    bj=konek_upacara.getBaju();
                    wktm=konek_upacara.getWaktu();

                    judul.setText(gjudul);
                    tanggal.setText(tgl);
                    tempat.setText(tmp);
                    isi.setText(gisi);
                    baju.setText(bj);
                    waktu.setText(wktm);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
