package com.example.sihadir;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    DatabaseReference databaseReference;
    String idAdnroid;


    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        setContentView(R.layout.activity_main);
        idAdnroid = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);
        autoLogin();
    }

    public  void autoLogin()
    {
        databaseReference = FirebaseDatabase.getInstance().getReference("DataLogin");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int a=0;
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    konek_login dataLogin1 = postSnapshot.getValue(konek_login.class);
                    if(dataLogin1.getNoAndroid().equals(idAdnroid))
                    {
                        a=1;
                    }
                }
                if(a==1)
                {
                    new  Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(MainActivity.this,Menu.class);
                            startActivity(intent);
                            finish();

                        }
                    },500);
                }
                else
                {
                    new  Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(MainActivity.this,Masuk.class);
                            startActivity(intent);
                            finish();

                        }
                    },500);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
