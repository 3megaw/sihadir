package com.example.sihadir.Sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqlLite extends SQLiteOpenHelper {
    public static final String DATABASE_NAME="data_pegawai.db";
    public static final String TABLE_NAME="table_mhs";
    public static final String COL_1="nip";
    public static final String COL_2="nama";
    public static final int DATABASE_VERTION=1;

    public SqlLite (Context context)
    {
        super(context,DATABASE_NAME,null,DATABASE_VERTION);
        SQLiteDatabase db = this.getWritableDatabase();
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table table_mhs(nip text);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_NAME + "'");
        onCreate(db);
    }

    public boolean insertData(String nip,String nama)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,nip);
        contentValues.put(COL_2,nama);
        long result = db.insert(TABLE_NAME,null,contentValues);
        if(result==-1) {
            return false;
        }else
        {
            return true;
        }

    }

    public Cursor getAllData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from data_pegawai",null);
        return  res;
    }
    public Cursor getData(String nip)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql="select * from table_mhs WHERE nip = '"+nip+"'";
        Cursor res = db.rawQuery(sql,null);
        return  res;
    }
    public boolean updateData(String nip)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,nip);
        db.update(TABLE_NAME,contentValues,"nip=?",new String[]{nip});

        return  true;
    }

    public int deleteData(String nip)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        return  db.delete(TABLE_NAME,"nip=?",new String[]{nip});
    }
}
