package com.example.sihadir;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.Query;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import  java.util.Collection;

public class daftarPengumuman extends AppCompatActivity {

    private List<DataPengumuman> data = new ArrayList<>();
    private DatabaseReference databaseReference;
    RecyclerView recyclerView;
    ListDaftarPengumuman listDaftarPengumuman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_pengumuman);

        onDaftar();
    }
    private void onDaftar(){
        recyclerView = findViewById(R.id.viewdaftar);
        listDaftarPengumuman = new ListDaftarPengumuman(this,data);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(listDaftarPengumuman);

        databaseReference = FirebaseDatabase.getInstance().getReference("Pengumuman");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    DataPengumuman dape =postSnapshot.getValue(DataPengumuman.class);
                    data.add(dape);
                    Collections.reverse(data);
                }

                listDaftarPengumuman.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
/*
    public void backbt(View view){
        Intent i = new Intent(this,pengumuman.class);
        startActivity(i);
    }

 */
}
