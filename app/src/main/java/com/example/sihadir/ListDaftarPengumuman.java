package com.example.sihadir;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class ListDaftarPengumuman extends RecyclerView.Adapter<ListDaftarPengumuman.ViewHolder>{
    @NonNull
    @Override
    public ListDaftarPengumuman.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater= LayoutInflater.from(viewGroup.getContext());
        android.view.View view = inflater.inflate(R.layout.listdaftarpengumuman,viewGroup,false);
        return new ListDaftarPengumuman.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListDaftarPengumuman.ViewHolder viewHolder, int i) {

        final DataPengumuman data1=data.get(i);
        viewHolder.tanggal.setText(data1.getTanggal());
        viewHolder.judul.setText(data1.getJudul());
        viewHolder.isi.setText(data1.getIsi());

        viewHolder.judul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("key", data1.getTanggal());
                bundle.putString("judul",data1.getJudul());
                bundle.putString("isi",data1.getIsi());
                bundle.putString("waktu",data1.getWaktu());
                bundle.putString("bagian",data1.getBagian());
                bundle.putString("tempat",data1.getTempat());

                Intent intent = new Intent(context,DetailPengumumanRec.class);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tanggal,judul,isi,lihat,id;

        public ViewHolder(android.view.View view){
            super(view);
            tanggal = (TextView) view.findViewById(R.id.tanggal);
            judul = (TextView) view.findViewById(R.id.judul);
            isi = (TextView) view.findViewById(R.id.isi);
       //     lihat = (TextView) view.findViewById(R.id.lihat);
        }
    }

    private Context context;
    private List<DataPengumuman> data;
    public ListDaftarPengumuman(Context context, List<DataPengumuman> data){
        this.context=context;
        this.data=data;
    }
}