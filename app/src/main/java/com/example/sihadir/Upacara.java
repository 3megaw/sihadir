package com.example.sihadir;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sihadir.Firebase.DataKegiatan;
import com.example.sihadir.Firebase.Konek_Upacara;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.core.OrderBy;
//import com.jaredrummler.materialspinner.MaterialSpinner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Upacara extends AppCompatActivity {

    private List<Konek_Upacara> data = new ArrayList<>();
    private DatabaseReference databaseReference;
    Konek_Upacara konek_upacara;

    RecyclerView recyclerView;
    ListUpacara listUpacara;
    TextView txttgl,txtjudul,txtisi,txttgl2,txtjudul2,txtisi2;

    Button buttonAll,buttonBelum,buttonAda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upacara);

        txttgl= findViewById(R.id.tgl);
        txtjudul= findViewById(R.id.judul);
        txtisi= findViewById(R.id.isi);
        txttgl2= findViewById(R.id.tgl2);
        txtjudul2= findViewById(R.id.judul2);
        txtisi2= findViewById(R.id.isi2);

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        final String formattedDate = df.format(c);

        //UPACARA TERBARU
        databaseReference = FirebaseDatabase.getInstance().getReference("Upacara");
        Query query = databaseReference.orderByKey().limitToLast(1);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    konek_upacara  = postSnapshot.getValue(Konek_Upacara.class);
                    String tampungjudul,tampungtanggal,tampungisi;

                    tampungtanggal = konek_upacara.getTanggal();
                    tampungisi = konek_upacara.getIsi();
                    tampungjudul = konek_upacara.getJudul();
               //     if (tampungtanggal.equals(formattedDate)){
                    txtjudul.setText(tampungjudul);
                    txttgl.setText(tampungtanggal);
                    txtisi.setText(tampungisi);}
                 //   else{
                }
                   // }
                //}
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        //UPACARA BERLANGSUNG
        databaseReference = FirebaseDatabase.getInstance().getReference("Upacara");
        Query query2 = databaseReference.orderByChild("Tanggal").equalTo(formattedDate);
        query2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    konek_upacara  = postSnapshot.getValue(Konek_Upacara.class);
                    String tampungjudul,tampungtanggal,tampungisi;

                    tampungtanggal = konek_upacara.getTanggal();
                    tampungisi = konek_upacara.getIsi();
                    tampungjudul = konek_upacara.getJudul();
                    //     if (tampungtanggal.equals(formattedDate)){
                    txtjudul2.setText(tampungjudul);
                    txttgl2.setText(tampungtanggal);
                    txtisi2.setText(tampungisi);}
                //   else{
            }
            // }
            //}
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        txtjudul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Upacara.this,DetailUpc2.class);
                startActivity(i);
            }
        });

        txtjudul2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Upacara.this,DetailUpc3.class);
                startActivity(i);
            }
        });

        semua();
/*
        setOnSemua();
        setOffAda();
        setOffBelum();
        buttonAda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sedangLangsung();
                setOnAda();
                setOffSemua();
                setOffBelum();

            }
        });
        buttonBelum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                berlangsung();
                setOnBelum();
                setOffSemua();
                setOffAda();
            }
        });
        buttonAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                semua();
                setOnSemua();
                setOffAda();
                setOffBelum();
            }
        });

 */
    }
    private void semua()
    {
        recyclerView = findViewById(R.id.DaftarUPC);
        listUpacara = new ListUpacara(data,Upacara.this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(listUpacara);


        databaseReference = FirebaseDatabase.getInstance().getReference();
        Query query = databaseReference.child("Upacara").orderByChild("Tanggal");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    Konek_Upacara konek_upacara = postSnapshot.getValue(Konek_Upacara.class);
       //               if(cekTanggal2(konek_upacara.getTanggal())==1){
                        data.add(konek_upacara);
                        Collections.reverse(data);
         //       }
                }
                listUpacara.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void berlangsung()
    {
        //RecycleView
        recyclerView = findViewById(R.id.DaftarUPC);
        listUpacara = new ListUpacara(data,Upacara.this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(listUpacara);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();

        databaseReference = FirebaseDatabase.getInstance().getReference();
        Query query = databaseReference.child("Upacara").orderByChild("Tanggal").startAt(dateFormat.format(date));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    Konek_Upacara konek_upacara = postSnapshot.getValue(Konek_Upacara.class);
                    if(cekTanggal(konek_upacara.getTanggal())==1)
                    {
                        data.add(konek_upacara);
                        Collections.reverse(data);

                    }
                }

                listUpacara.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void sedangLangsung()
    {
        //RecycleView
        recyclerView = findViewById(R.id.DaftarUPC);
        listUpacara = new ListUpacara(data,Upacara.this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(listUpacara);
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date date = new Date();

        databaseReference = FirebaseDatabase.getInstance().getReference();
        Query query = databaseReference.child("Upacara").orderByChild("Tanggal").equalTo(dateFormat.format(date));
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    Konek_Upacara konek_upacara = postSnapshot.getValue(Konek_Upacara.class);
                    data.add(konek_upacara);

                }

                listUpacara.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private int cekWaktu(String waktu)
    {
        int a=0;
        Calendar calendar =Calendar.getInstance();
        SimpleDateFormat simpleDateFormat =new SimpleDateFormat("h:m");
        Date tanggal=null,tanggal2 = null;
        try {
             tanggal =simpleDateFormat.parse(simpleDateFormat.format(calendar.getTime()));
             tanggal2 = simpleDateFormat.parse(waktu);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(tanggal.equals(tanggal2))
        {
            a=2;
        }
        else if(tanggal2.after(tanggal))
        {
            a=1;
        }
        return a;
    }
    private int cekTanggal(String tanggal)
    {
        int a=0;
        Calendar calendar =Calendar.getInstance();
        SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy/MM/dd");
        Date tanggal1=null,tanggal2 = null;
        try {
            tanggal1 =simpleDateFormat.parse(simpleDateFormat.format(calendar.getTime()));
            tanggal2 = simpleDateFormat.parse(tanggal);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if((tanggal1.equals(tanggal2)))
        {
            a=2;
        }
       else if(tanggal2.after(tanggal1))
        {
            a=1;
        }
        return a;
    }
    private int cekTanggal2(String tanggal)
    {
            int a=0;
            Calendar calendar =Calendar.getInstance();
            SimpleDateFormat simpleDateFormat =new SimpleDateFormat("yyyy/MM/dd");
            Date tanggal1=null,tanggal2 = null;
            try {
                tanggal1 =simpleDateFormat.parse(simpleDateFormat.format(calendar.getTime()));
                tanggal2 = simpleDateFormat.parse(tanggal);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if((tanggal1.equals(tanggal2))|| tanggal1.before(tanggal2))
            {
                a=1;
            }

            return a;

    }
    private void setOnSemua()
    {
        Resources resource =getResources();
        Drawable drawable= ResourcesCompat.getDrawable(resource,R.drawable.btn_biru,null);
        buttonAll.setBackground(drawable);
        buttonAll.setTextColor(getResources().getColor(R.color.putih));
    }
    private void setOffSemua()
    {
        Resources resource =getResources();
        Drawable drawable= ResourcesCompat.getDrawable(resource,R.drawable.btn_putih,null);
        buttonAll.setBackground(drawable);
        buttonAll.setTextColor(getResources().getColor(R.color.hitam));
    }
    private void setOnAda()
    {
        Resources resource =getResources();
        Drawable drawable= ResourcesCompat.getDrawable(resource,R.drawable.btn_biru,null);
        buttonAda.setBackground(drawable);
        buttonAda.setTextColor(getResources().getColor(R.color.putih));
    }
    private void setOffAda()
    {
        Resources resource =getResources();
        Drawable drawable= ResourcesCompat.getDrawable(resource,R.drawable.btn_putih,null);
        buttonAda.setBackground(drawable);
        buttonAda.setTextColor(getResources().getColor(R.color.hitam));
    }
    private void setOnBelum()
    {
        Resources resource =getResources();
        Drawable drawable= ResourcesCompat.getDrawable(resource,R.drawable.btn_biru,null);
        buttonBelum.setBackground(drawable);
        buttonBelum.setTextColor(getResources().getColor(R.color.putih));
    }
    private void setOffBelum()
    {
        Resources resource =getResources();
        Drawable drawable= ResourcesCompat.getDrawable(resource,R.drawable.btn_putih,null);
        buttonBelum.setBackground(drawable);
        buttonBelum.setTextColor(getResources().getColor(R.color.hitam));
    }
}
