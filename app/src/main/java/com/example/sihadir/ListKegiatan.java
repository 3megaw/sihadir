package com.example.sihadir;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.sihadir.Firebase.DataKegiatan;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ListKegiatan extends RecyclerView.Adapter<ListKegiatan.KegiatanViewHolder> {

    @NonNull
    public ListKegiatan.KegiatanViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.listkegiatan, viewGroup, false);
        return new ListKegiatan.KegiatanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListKegiatan.KegiatanViewHolder kegiatanViewHolder, int i)
    {


        final DataKegiatan dataKgt = datakegiatan.get(i);
        kegiatanViewHolder.judul.setText(dataKgt.getJudul());
        kegiatanViewHolder.tanggal.setText(dataKgt.getTanggal());
        kegiatanViewHolder.isi.setText(dataKgt.getIsi());
        kegiatanViewHolder.judul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("key",dataKgt.getId());
                bundle.putString("judul",dataKgt.getJudul());
                bundle.putString("isi",dataKgt.getIsi());
                bundle.putString("tanggal",dataKgt.getTanggal());
                bundle.putString("tempat",dataKgt.getTempat());
                bundle.putString("wktm",dataKgt.getWaktumulai());
                bundle.putString("wkta",dataKgt.getWaktuselesai());
                bundle.putString("tag",dataKgt.getTag());

                Intent intent = new Intent(context,DetailKegiatan.class);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount()
    {
        return datakegiatan.size();
    }


/*
    @Override
    public void onClick(View view) {
    }
*/

    public class KegiatanViewHolder extends RecyclerView.ViewHolder
    {
        TextView judul;
        TextView tanggal;
        TextView isi;

        public KegiatanViewHolder(@NonNull View itemView)
        {
            super(itemView);
            tanggal = (TextView) itemView.findViewById(R.id.tgl);
            judul = (TextView) itemView.findViewById(R.id.judul);
            isi = (TextView) itemView.findViewById(R.id.isi);
        }
    }

    private List<DataKegiatan> datakegiatan;
    private Context context;

    public ListKegiatan(List<DataKegiatan> datakegiatan, Context context)
    {
        this.datakegiatan = datakegiatan;
        this.context=context;
    }
}
