package com.example.sihadir;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.sihadir.Firebase.DataKehadiranApelPagi;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class DetailPengumuman extends AppCompatActivity {

    TextView txtjudul,txtdivisi,txttanggal,txtjam,txtlokasi,txtdeskripsi,txt5dash;

    DatabaseReference databaseReference;
    DatabaseReference databaseReference2;
    DatabaseReference databaseReference3;
    DatabaseReference databaseReference4;
    DataPengumuman DataPengumuman;
    konek_datadiri konekDatadiri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pengumuman);

        konekDatadiri = new konek_datadiri();

        DataPengumuman=new DataPengumuman();

        txtjudul=findViewById(R.id.judul_detailpengumuman);
        txtdivisi=findViewById(R.id.pengumuman_divisi);
        txttanggal=findViewById(R.id.tanggal);
        txtjam=findViewById(R.id.jam);
        txtlokasi=findViewById(R.id.txtlokasi_pengumuman);
        txtdeskripsi=findViewById(R.id.deskripsi_pengumuman);
        txt5dash=findViewById(R.id.textView5);

        /*
        databaseReference = FirebaseDatabase.getInstance().getReference("Kehadiran Apel Pagi");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
         //       data.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    databaseReference2=databaseReference.child(postSnapshot.getKey());
                    databaseReference2.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot ff : dataSnapshot.getChildren()){
                                String cekx;
                                DataKehadiranApelPagi dataKehadiranApelPagi = ff.getValue(DataKehadiranApelPagi.class);
                                if (dataKehadiranApelPagi.getNip().equals(konekDatadiri.getNip())){
                                    cekx=dataKehadiranApelPagi.getNip();
                                    txtdeskripsi.setText(cekx);
                                }
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
//                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

         */
/*
        //RECYCLER
        Intent i = this.getIntent();
        String judul=i.getExtras().getString("judul");
        String tgl=i.getExtras().getString("key");

        txt5dash.setText(judul);


 */

        databaseReference = FirebaseDatabase.getInstance().getReference("Pengumuman");
        Query query = databaseReference.orderByKey().limitToLast(1);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    DataPengumuman=postSnapshot.getValue(DataPengumuman.class);
                    String tampungjudul,tampungdivisi,tampungtanggal,tamgpungjam,tampunglokasi,tampungdeskripsi;
                    tampungjudul=DataPengumuman.getJudul();
                    tampungdivisi=DataPengumuman.getBagian();
                    tampungtanggal=DataPengumuman.getTanggal();
                    tamgpungjam=DataPengumuman.getWaktu();
                    tampunglokasi=DataPengumuman.getTempat();
                    tampungdeskripsi=DataPengumuman.getIsi();

                    txtjudul.setText(tampungjudul);
                    txtdivisi.setText(tampungdivisi);
                    txttanggal.setText(tampungtanggal);
                    txtjam.setText(tamgpungjam);
                    txtlokasi.setText(tampunglokasi);
                    txtdeskripsi.setText(tampungdeskripsi);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
