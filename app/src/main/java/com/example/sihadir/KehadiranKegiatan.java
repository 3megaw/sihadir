package com.example.sihadir;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sihadir.Firebase.DataKehadiranApelPagi;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

public class KehadiranKegiatan extends Fragment {
    View view;

    private List<DataKehadiranApelPagi> data = new ArrayList<>();
    private DatabaseReference databaseReference;
    private DatabaseReference databaseReference2;
    private DatabaseReference databaseReference3;
    private DatabaseReference databaseReference4;
    RecyclerView recyclerView;
    HistoryApelPagi historyApelPagi;

    konek_datadiri konekDatadiri;

    public  View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState)
    {
        view=  inflater.inflate(R.layout.kegiatan_profil,container,false);
        MaterialSpinner spinner = view.findViewById(R.id.radio);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(),R.array.dropdwon,R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        pagi();

        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                String item2 =(String) item;
                if(item.equals("Kehadiran Apel Pagi"))
                {
             //       pagi();
                }
                else if(item.equals("Kehadiran Apel Sore"))
                {

                }
               else if(item.equals("Kehadiran Upacara/Apel"))
                {

                }
            }


        });

        return  view;
    }
    private void pagi()
    {
        //RecycleView
        recyclerView = view.findViewById(R.id.DaftarKegiatan);
        historyApelPagi = new HistoryApelPagi(data,view.getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(historyApelPagi);

        databaseReference = FirebaseDatabase.getInstance().getReference("Kehadiran Apel Pagi");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    databaseReference2=databaseReference.child(postSnapshot.getKey());
                    databaseReference2.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot ff : dataSnapshot.getChildren()){
                                DataKehadiranApelPagi dataKehadiranApelPagi = ff.getValue(DataKehadiranApelPagi.class);
                                if (dataKehadiranApelPagi.getNip().equals(konekDatadiri.getNip())){
                                    data.add(dataKehadiranApelPagi);
                                }
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
                historyApelPagi.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void sore()
    {
        //RecycleView
        recyclerView = view.findViewById(R.id.DaftarKegiatan);
        historyApelPagi = new HistoryApelPagi(data,view.getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(historyApelPagi);

        databaseReference = FirebaseDatabase.getInstance().getReference("Kehadiran Apel Pagi");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    DataKehadiranApelPagi dataKehadiranApelPagi = postSnapshot.getValue(DataKehadiranApelPagi.class);
                    data.add(dataKehadiranApelPagi);
                }

                historyApelPagi.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void kegiatan()
    {
        //RecycleView
        recyclerView = view.findViewById(R.id.DaftarKegiatan);
        historyApelPagi = new HistoryApelPagi(data,view.getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(historyApelPagi);

        databaseReference = FirebaseDatabase.getInstance().getReference("Kehadiran Apel Pagi");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    DataKehadiranApelPagi dataKehadiranApelPagi = postSnapshot.getValue(DataKehadiranApelPagi.class);
                    data.add(dataKehadiranApelPagi);
                }

                historyApelPagi.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void upc()
    {
        //RecycleView
        recyclerView = view.findViewById(R.id.DaftarKegiatan);
        historyApelPagi = new HistoryApelPagi(data,view.getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(historyApelPagi);

        databaseReference = FirebaseDatabase.getInstance().getReference("Kehadiran Apel Pagi");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                data.clear();
                for(DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {
                    DataKehadiranApelPagi dataKehadiranApelPagi = postSnapshot.getValue(DataKehadiranApelPagi.class);
                    data.add(dataKehadiranApelPagi);
                }

                historyApelPagi.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
